#!/bin/bash

# Create directory
mkdir -p ~/.pandoc/templates

# Link at ./pandoc
stow -Rt ~/.pandoc/templates -d pandoc templates
