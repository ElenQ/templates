(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix gexp)
             (guix build-system copy))

(define %source-dir (dirname (current-filename)))

(define-public templates
  (package
    (name "templates")
    (version "1.0.0")
    (source (local-file %source-dir #:recursive? #t))
    (build-system copy-build-system)
    (arguments (list #:install-plan ''(("pandoc" "/share/pandoc"))))
    (native-search-paths
     (list (search-path-specification
            (variable "XDG_DATA_HOME")
            (files '("share")))))
    (home-page "http://git.elenq.tech/")
    (synopsis "Pandoc templates for ElenQ Technology")
    (description "A set of well configured Pandoc templates for ElenQ
Technology's documents.")
    (license license:asl2.0)))

templates
