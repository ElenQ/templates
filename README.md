# ElenQ Documentation templates

`pandoc` templates for ElenQ documents.

## Installation

Run `install.sh` script and it'll automagically link all the templates to your
`~/.pandoc/templates/` folder.

This method doesn't work on a non-Unix-like OS.


## Usage

Once installed `pandoc` will be aware of the place where the templates are so
they can be used directly like:

```
pandoc input.md -o output.pdf --template=elenq-article
```

### Specific details

These templates only work with `pandoc`'s `--latex-engine=xelatex` option.

> NOTE: Recent `pandoc` versions changed the name of the option to `--pdf-engine`

### Metadata

Templates expect some metadata to be set, no field is mandatory but some can be
important (judge yourself):

``` yaml
lang: english | spanish | basque | ...
title: The title of the document
subtitle: The subtitle of the article
author: Author's name
license: License of the document
```

Templates also accept any metadata valid for Pandoc.

> NOTE: Templates automatically put the copyleft symbol in the PDF, they are
> only prepared for copyleft licenses.

### Helpful help

You'd probably want to create some aliases like these:

``` bash
# Pandoc for ElenQ
alias pandoc-xelatex="pandoc $@ --pdf-engine=xelatex --to latex -N"
alias elenqdoc-article="pandoc-xelatex $@ --standalone --template elenq-article --metadata=documentclass:article"
alias elenqdoc-book="pandoc-xelatex $@  --standalone --template elenq-book --metadata=documentclass:book --top-level-division=chapter"
```

## Editing

Just edit the content of the templates as they were LaTeX files.

### Logos

The ElenQ Technology logo is embedded inside the templates as a `tikz` image.

There's a way to convert from SVG to TikZ you'd probably want to use to change
the logo from the templates.

https://github.com/kjellmf/svg2tikz

## License

This repository is distributed under the terms of the Apache License 2.0.
